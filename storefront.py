
import dbmanager as DB

########################################################################################
#Establish interfaces


menu = 'login'
try:
    while True:
        match menu:
            case 'login':
                action = int(input("---------------------\nWelcome, would you like to:\n1: Login\n2: Register\n"))
                if action == 1:
                    action = 0
                    username = input("---------------------\nLogin\nUsername: ").capitalize()
                    if username.lower() == 'admin':
                        print('Welcome, Admin!')
                        menu = 'admin home'
                    else:
                        # Check username against known customers
                        customerstable = DB.session.query(DB.t_customers).all()
                        users = {}
                        for item in customerstable:
                            users[item[1]] = 1
                        if username.capitalize() in users.keys():    
                            menu = 'home'
                        else:
                            # make unknown customers add themselves
                            print('---------------------\nCustomer not found, please register')
                            DB.add_customer()
                elif action == 2:
                    action = 0
                    username = input("---------------------\nRegister\nUsername: ")
                    DB.add_customer()
            case 'home':
                action = int(input("---------------------\nWould you like to:\n1: Buy an item\n2: Check purchase history\n3: Logout\n"))
                if action == 1:
                    action = 0
                    menu = 'market'
                elif action == 2:
                    action = 0
                    print('---------------------\nPurchase History')
                    DB.view_receipts(username)
                elif action == 3:
                    break
            case 'market':
                print('---------------------\nMarket')
                products = DB.session.query(DB.t_products)
                for item in products.all():
                    print(f'{item[0]}: {item[1]} - ${item[2]}')
                action = int(input("Please select from the above:\n"))
                item = products.all()[action-1][1]
                DB.purchase(username, item)
                print(f"Thank you for your purchase of {item}")
                menu = 'home'
            case 'admin home':
                action = int(input("---------------------\nWould you like to:\n1: View Purchase records\n2: Add/Remove Users\n3: Add/Remove Products\n4: Logout\n"))
                if action == 1:
                    action = 0
                    print('---------------------\nPurchase Histories')
                    DB.view_all_receipts()
                elif action == 2:
                    action = 0
                    menu = 'admin users'
                elif action == 3:
                    action = 0
                    menu = 'admin products'
                elif action == 4:
                    break
            case 'admin users':
                print('---------------------\nCurrent Users:')
                customers = DB.session.query(DB.t_customers)
                for item in customers.all():
                    print(f'Name: {item[1]}  #{item[2]}')
                action = int(input('---------------------\nWould you like to:\n1: Add Users\n2: Remove Users\n3: Return\n'))
                if action == 1:
                    print('---------------------\nAdd Customer')
                    DB.add_customer()
                    action = 0
                if action == 2:
                    print('---------------------\nRemove Customer')
                    DB.remove_customer()
                    action = 0
                if action == 3:
                    action = 0
                    menu = 'admin home'
            case 'admin products':
                print('---------------------\nCurrent Products:')
                products = DB.session.query(DB.t_products)
                for item in products.all():
                    print(f'{item[0]}: {item[1]} - ${item[2]}')
                action = int(input('---------------------\nWould you like to:\n1: Add Products\n2: Remove Products\n3: Return\n'))
                if action == 1:
                    print('---------------------\nAdd Product')
                    DB.add_product()
                    action = 0
                if action == 2:
                    print('---------------------\nRemove Product')
                    DB.remove_product()
                    action = 0
                if action == 3:
                    action = 0
                    menu = 'admin home'
except Exception as e:
    print(f'Sorry, it seems you broke this program:\n{e}')